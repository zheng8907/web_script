// ==UserScript==
// @name              全网VIP视频会员
// @namespace         http://tampermonkey.net/
// @version           2.1.2
// @description       全网VIP视频免费观看，支持：腾讯、爱奇艺、优酷、芒果、pptv、乐视等其它网站；支持PC端和移动端。
// @icon              data:image/jpeg;base64,/9j/4QC8RXhpZgAASUkqAAgAAAAGABIBAwABAAAAAQAAABoBBQABAAAAVgAAABsBBQABAAAAXgAAACgBAwABAAAAAgAAABMCAwABAAAAAQAAAGmHBAABAAAAZgAAAAAAAAAAdwEA6AMAAAB3AQDoAwAABgAAkAcABAAAADAyMTABkQcABAAAAAECAwAAoAcABAAAADAxMDABoAMAAQAAAP//AAACoAQAAQAAAMgAAAADoAQAAQAAAJoAAAAAAAAA/9sAQwAIBgYHBgUIBwcHCQkICgwUDQwLCwwZEhMPFB0aHx4dGhwcICQuJyAiLCMcHCg3KSwwMTQ0NB8nOT04MjwuMzQy/9sAQwEJCQkMCwwYDQ0YMiEcITIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIy/8AAEQgAmgDIAwEiAAIRAQMRAf/EAB8AAAEFAQEBAQEBAAAAAAAAAAABAgMEBQYHCAkKC//EALUQAAIBAwMCBAMFBQQEAAABfQECAwAEEQUSITFBBhNRYQcicRQygZGhCCNCscEVUtHwJDNicoIJChYXGBkaJSYnKCkqNDU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6g4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqys7S1tre4ubrCw8TFxsfIycrS09TV1tfY2drh4uPk5ebn6Onq8fLz9PX29/j5+v/EAB8BAAMBAQEBAQEBAQEAAAAAAAABAgMEBQYHCAkKC//EALURAAIBAgQEAwQHBQQEAAECdwABAgMRBAUhMQYSQVEHYXETIjKBCBRCkaGxwQkjM1LwFWJy0QoWJDThJfEXGBkaJicoKSo1Njc4OTpDREVGR0hJSlNUVVZXWFlaY2RlZmdoaWpzdHV2d3h5eoKDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uLj5OXm5+jp6vLz9PX29/j5+v/aAAwDAQACEQMRAD8A9/ooooAKKK4Dxr40n0+5bS9LkCTKP38+ASmf4V7Zx1PbPr0mUlFXZrRoyrT5Inf5ozXz9JqeozOXk1C8dj1LXD/4037de/8AP5df9/3/AMay9uux6P8AZUv5j6DzRmvnz7de/wDP5df9/wB/8aPt17/z+XX/AH/f/Gl7fyD+yn/N+B9B5ozXz59uvf8An8uv+/7/AONH269/5/Lr/v8Av/jR7fyD+yn/ADfgfQeaM18+fbr3/n8uv+/7/wCNH269/wCfy6/7/v8A40e38g/sp/zfgfQeaM18+fbr3/n8uv8Av+/+NH269/5/Lr/v+/8AjR7fyD+yn/N+B9ASSCMAkHaTgn0p9eFabLeNcRXNxqc1vbRSBmklmc7sHO1Vz8x/DHrWr4g1VVt4ptIdkt73f5syu+7cDzHgn5OCDgYzmq9tpexm8uakoqX4Hr+eOKryfa/+WbQ/8CU14L9uvf8An8uv+/7/AONH269/5/Lr/v8Av/jU+38jVZW19r8D2i+W9CAzlTHn+Dpn3qa0XUPJTYUEePlEg7V5L4bv7geI7ATXM7xtJtZXlZgcg9iazH1G8kkeQXdyAzFsCdwBk59aParcf1CT9y6+497j+05/emLH+ypqevAIdV1K2kEkOoXkbjuJ3/qcV6Z4J8YSa1usNQK/bUXckgG0TL34/vD27c+tXCqpOxzYjATpR507o7Oim7wZCg6gZNOrU4AooooAKKKZJIkMTySMERAWZmOAAOpNAGN4q19PD+jvcDabmT93boe7+v0HU/8A168KurmR5XLOXkZi0jt1ZjyT9a3PFniJ9d1Z7oEi3TMdsh7L/ePuev5DtXM1xVZ8zPpsBhfY07y3YoDNnGTgFj9BSVp6JJZWuoQ3d/cbbeJjvhSIyPKpBDLgYABBPJP0FJrUVlaajNZ2FuVgibCzSSmR5VIBDZ4ABBHAH1NRbS51+0/eclvn0M2iilGAwyMj0zjNSaCUVq3w0saXZpatcRXRVppkkAkXLcKocYxgLnBX+LrWVTasTCfMr2sFKAzEhQSQCePQcmkrQ0d7W21GC8u7kRxQSBzGsRkeQDqoXpgjIySOvehasJy5Ytoz6K1ddj060v5LXTLci2AV455JS7SKyhhjoFGDjoTxyayqGrOwQnzxUrWuHfPfpUizutvJBnMTsHKnsw6Ee+CR9DUda1r/AGV/Y108/wBohvHZYYmXEqY+8zbeCOAFzk/e4FCVxTlyra5k0Up4JGc+9aGjQ2d5qNvZXtu7R3Eix+bFMY2jB6nkEEAc9PxoSu7DnLli5WKcMslpcRTLkOhEi5/Q1CBgADoBitXXZNPu9Snu9OuWaCRhshkhMZRQAoAPIIwBjofasuhqzsKnLmipWsxQxUgg103h1rS81K1+z3U+namjhoiVEschHYdCCRng5B9a5lVL7toztUs2OwH/AOsVf0i8Gm3b3+AZoI28hT/z1YbVP0GS34CnF2ZNaPNBpbnvmnTrJ5hdh5ztuIPHHbHtV+vFfBviJdN1UC8cvZ3TBZi5yUfs+e3Xn2PtXtKjCgAk/U5rtpz5kfM4vDuhOzFoooqzlCvPPiP4i2R/2HbPhmAe6Ydl6hPx6n2x6113iLWo9B0Wa+dQ7jCRIf43PQfTufYGvBb+6luZ5ZJpDJNKxeVz/ETzWFadlZHp5bhvaT9pLZfmVpZPMcnt2plIWCgkkAD1qWaF4Jmik4dcZHpkAj9CK5D6LbQjrXsfDWu6taJdWWnS3EByiyK6AfKcY5IPHSsivavhoR/whduOM+dNx/20NaU4KTszkxuIlQpqcVrc80/4QjxP/wBAWf8A77j/APiqP+EI8Tj/AJg0/wD33H/8VXveBTJCqockDIPU+1b/AFePc8r+1638q/H/ADPnHT9PvNWultrCBridlLhFIBIHU8keorW/4QjxP/0BZ/8AvuP/AOKq78MePF9uTwPssvX6LXtwwQCMYNZ06Smrs68bj6lCpyRStY+etQ8M63pVm13f6dLb26kAyMyEAngdCTVoeCfExAI0acg8j54//iq9I+KGP+EIuQMZ82Lj/gVdfAVaFMEHCjofaqVGPM0YyzOqqUZ2V2336W8/M8JPgrxSwUHRrg7RgfvI+B6fe96in8HeIra3knn0mZIokLu5dMKoGSeGr3/A9Ky/EZUeGdWGQCbKbjP+wap0IpbmcM2qyklyr8f8zwnTND1TWVlbTbKS5ERAfYyjbnp1I9Kv/wDCEeJ/+gLP/wB9x/8AxVdd8IyFh1csQBvh6/7rV6bj2qadGMo3ZtisxqUazpxSsvX/ADPBP+EI8T/9AWf/AL7j/wDiqVfBXihDuXR7gHBGQ6dxj+9XvEkkcMTyyuqRopZnY4CgdST2FUbfXdHvJlhtdVsZ5W+6kVwjE/QA1XsI9zD+1a7+yvx/zPCbjw1rtohefRr5EHVhCWA/75zWX1/lX01iuQ8aeDbXW7Ca8tYVj1SNS6ugx52P4W9c9j1Bx2qZULK6N6Gbc0lGoreZ5RbaskGkXNpLaW9w07Im502ukY+YjeuDy23rnpWYcZOBgduc00EEAjoeacVIUMQcHIB9cf8A6xXO22evGEYttdR8MmxsH7rcGvYvh94i/tCwOl3L5urVf3ZPWSLoPxHQ+2K8ZrT0jUrjTr6C7tn23EDbkz0YdwfYjg/Wrpz5Wc2Mwyr07deh9EUVT0nUodX0u3v7fPlzJuAPVT0IPuDkfhRXdufLNNOzON+LAlTw/Z3CZKRXQ3D3KMAT+PH415Cr7+e/evpLUdPttV0+exvIhJbzLtdT6f0I65rwTxV4WvPCmpeW5aW0kJ+z3GOHH91vRh+vUe3NWg78x7eV4iKj7J7mZFPLbsXhcK+OCVVh+TAir2s6vNq9/LPJ5e1iNoESKQAAOoGT09azVYMMj8qWue7tY9fki5KdtQq1ompNoHiWz1kY8uFwJgOpjPyv+hz9QKghhkuLmG2iXM0zqkad2JOBTZUUPJHkOoJXPYjOM002ncVSMakXBn0wrK6B0YMrDIIOQRXIfEvRRrHg64ZEDT2bC6j47L94f98lqj+GWtHU/CiWkr7rnTm+zPk8lByh/wC+eP8AgJrspESSNkdQyMCGB6EHrXdpOJ8n71CtrumfOWmaQdf1ez0tR8txMoc+iDlj/wB8g19GKscEIVQscSLgDoFAH+FedfD/AMMNpnibWJplJWxc2kDEdc4bP/fGz/vo1s/EnWDpXhGeGJttzfn7LHg8gN98/goP5isqS5Its7sfP6xXjCHl+J49repNr3ia81k48uaQiIHqIx8qfoM/ia734NQeSNc+78xg6fR683ACqFUYAGBXp3wi6az9YP5PWVJ3qHoY6nGGEaXS35o9Nrx34uweZ4k0t/l+W1br/v17FXkfxY/5D2n/APXq3/oddFb4DyctV8Qvn+R58YPOurY/L8sq9f8AeFfTtfNEH/HxD/10T/0IV9L1nh+p15wrOD9f0KGuQvc+H9SgiQvJJayoqqMkkoQAK8Gtfh94lnkiSHS54ZRgrLKojEZH8Wc8Y68V9DSSJFG8kjqkaAszMcAAdST6Vm/8JJoPX+2tN/8AAqP/ABrWcFJ6s4cNiZ0YtRje5pqCFAY7iByfWkd1jQu7BVUEknoBWJdeM/DNom6XXdPx6JOrn8lya868afEldYtZNI0JZVt5gUnunUoXXuqDqAR1Jxx2705TjFGdLDVKskkjiXdZJHdOEdiy/QnI/Suh0qLR20i9tr2Z7q6VTdww2+UAKKdy+YRySvUAfw8HNc504HSlR2Rw6khgcgiuGMrM+qqU+eNk7DpXWSQskSRKeiISQPxJJP1qMyeUQw+92pGYIvv2FdH4N8G3Him886ffFpkTYllHBkP9xP6nt9aIxcnZCq1Y0oXk9D0r4aJKPBsEsgIWaaWRAf7pb/EGiust7eK1t47eCNY4YlCIijAVQMACiu+KsrHyVWfPNy7klU9V0uz1nTpbG+hEsEowR3B7EHsR2NXKKZKbTuj548T+Gb3wpqfkTZktpCTb3GMCQeh9GHcfiKylYMMivo7V9Istc02Wwv4hJDIPoVPZlPYj1rwPxL4bvvCuqfZrj95DJk29wBhZV/ow7j8elclWly6rY+hwOOVVck/i/Mt+H9fudGkmlEgkjjhYxQSjcvmnAUjuuMknGMgEVlXM0M8m+K0jts9UiZin4BskfTNV1YMMilrJydrHfGlBTc1uzpvh9rH9j+MYEdsW+or9lk9A/WM/nlf+BV7pXzHIrMhCMVcYZGHVWHIP517honj7Qr3RbO4vtXsbW7eIedDLOqMj9G4J6ZBx7V0UJq1meNmmHfOqkVudWFVSxAALHJwOp6f0rxT4lav/AGn4uNojZg02PyuDwZWwXP4DaPzr0i98d+G7WwuLiLWdPnkijZ1hiuFZpCBwoAPJJ4rwnzJZmee4bdcTO0srerscn9TTrzVrInK8O3Uc5LYRjtUn0Ga9r8B+Fb3w0l+15Nbyi68sp5JbjaGznIH94V4o4yjAdSDXukfxA8KLGinXbTIUA/Mf8KzoJXuzrzSVTkUILR7/ACsdPXn/AMRPC17q8i6rBNbpBZWkhkWQtuODu4wMdBW3/wALB8J/9B20/wC+j/hVDWvHPhi70HUbaDWrWSaa1ljjQE5ZipAHT1rom4yjZs8jDqtSqqUYv7jznwv4VvvEryyWc1vGLWSMuJiwJyc8YB9K96ryD4ceIdI0GLUhql/DamZojGJCfmwGzj867r/hYPhP/oO2n/fR/wAKijyqNzqzL2tSs42ultp6G1qto9/o99ZxsqvcW8kSlugLKQM+3NfPev8Ag+58M3kNneTWskkkIkBhUkAZx3A9K9q/4WD4T/6Dtp/30f8ACvN/iFrOna3rlrc6ZeRXUKW2xmjPAbeTj8jSrWauh5aqkavJJaM4kWYXq+B7DFTrCICV2kMOuetbfh6PSbnUY4tUglEUYaZ5opcDagLEOpByDjHBB5qlqXkSXs1xb3guEmkaT5o2jddxJwVPHfsSK52tLntRlFVORIp01nCDJ69qGYIMn8BW94P8I3XivUCzF4tOhbE846k/3E/2v5D8BSjFt2Q6tWNKLlJjvB3g+58VXxklLxabC2JphwXP9xPf1Pb64r3azs7ewtIrW0hSGCJQqRoMBRSWVlbadZRWdnCkNvCu1I0HAH+e/erFdsIKKPmcVipV5Xe3QKKKKs5QooooAKz9Z0ay17TJbC/i3wvyCOGRuzKexH+eK0KKNxptO6PnPxF4evvC2qm0uhvjfJgnAwsy/wBCO47fSs9WDDIr6K13Q7LxBpkljfR7o25Vl4aNuzKexH/1q8D1/QL7wxqrWd4NynJhmUYWZfUeh9R2+mK46lPl1Wx9Fgcaqq5ZfF+ZSqBrUM5YORnnGKmDBhkVsaFoX9uX8FvHeWylmBljdikgQfeKgjDcehrOKbdkd1WUIx5p7IwBaAEEuSM9MVYqzfW1xaXksdzayWzliRG64wM8Y9RjuKgSNnDlRnYpdsdgMZP60ne5UeW10NqD7Kv/AD0ap6KBuKe5B9lH/PRqVbYKwO9uDmtzQ7KwvLlmvLs26QI07+ZHmNlXoNwORyV7HPbmsuSMxNsaSOQj+OJ9yn3Bp62uZrkcnFLVEMsQlIyxGPSmfZR/z0ap609A0uXWNUS0SGR0kDK8irkQ5B2uT2w2Pr0oV27IdTkjFzl0MX7KP+ejVJHGIlIDE5Oeav6hp502draa5gluY2KyJBlhGR1BYgDPsM1TpO60Y4crXNEVXZQwU4DDa3uOuP0FMZgoyaVmCDJrX8K+FrzxXqRjQtFZxEfaLjH3R/dX1Y/p1Pu0m3ZCq1Y04uUh/hLwnd+K9RIy0VjEw+0XAHT/AGF/2j+nU9gfedP0+10uxhsrKFYbeFdqIvb/ABPv3pumabaaRYRWVlCsUEQwqj+Z9Se571brshBRR8xisVKvLy6BRRRWhyhRRRQAUUUUAFFFFABWXr+gWXiLS3sb1MqfmSRfvRt2ZT6/z6VqUUmr6McZOLutz5v1zQ77w1qr2V6uf4o5VHyzL/eH9R2qG2umgWcxHDTQtCW7hWxu/MDH4mvf/EXh6y8SaW9leLg/eilX70TdmH+HevBNa0a+8N6q9jfJhxyjr92VezL/AIdq5KlNxd0fRYPGRrx5Z7lfcdgTJ2joueB9BWr4Yjkl8S2MUcBnDybJowMgxMCr59BtJrIBBGRyK0Y9c1G3tvs1pctaQdSlqBFuPqzD5mP1NZxaTuztqxlKDjHqa2seEoNBlb7drEKwtkwRxxNJPIuePl4A9Mk4rm5DGXPlBwnbeQT+OOK1tX1FtYsNPu55C95ArWs7N1cA7o3/ABBYH3HvWPTny391aE4ZVOT967sUMyqwBIDYB9+c/wA6SrFra/a5PLW4gikP3RO+xW9tx4B+uPrW7YeGb+wkudQ1awkhtNPhaciTBEzgfIoIJyM4JPTA96Si3sVUrQp/E9e3f0OaIwcHrSl3aLyi7+XnO3ccZ9cevvSfMeWOW7n1PeipNbE95dPe3clzLzLJhpD/AHmwAT+OM/Umq7MFGTQzBRk1p+GvDV74q1T7PBmOCPBuLgjKxL6D1Y9h+J4qknJmc5xpQu9Eh3hfwxeeK9T8mLMdrGQbi4xxGPQerHsPxNe9aVpdpo2nRWNjCIoIhgKOpPck9ye5pNI0iz0TTYrCxiEcMY+pY92J7k+tXq7KdNRXmfM4vFyry8gooorQ5AooooAKKKKACiiigAooooAKKKKACsfxJ4bsvE2mNZ3Y2uvzQzKPmib1H9R3rYopNX0ZUZOL5o7nzZrGkX3h3VZLC/j2yLyrL92VezKfT+XQ1WBBGR0r6C8TeGbLxPphtLobJVy0E6j5om9R6j1Hf8jXgmq6VfeH9UksL+PZKnII+7IvZlPcH/6xrkqU+V6bH0eCxqrKz3IaKAQRkdK0NL0e91W4WO3tZ5EIYmRIyVGFJ69O2PxrJJt2R3SnGMeaT0M+uvj8T3Wg+G9N0yBYZ5ZI2mnW5TeqRuSUjxnuvJ9ARXJSRSxHZPDJE+OUkQqR+Bod2kcu5JY9SaqMnHYzq0YVrKWqWpZvZ7O5bzLeyNnIfvRxyF4j7qDyv0yR9KpswUZNDMFGTWh4e8PX3ijVBaWo2IuDNORlYV9fcnsO/wBM0knJlSlGlG7eiHeG/Dl94q1QWtt+7hTBnuCMrEv9WPYf0r3vR9HstC02KwsIvLhT15Z27sx7k+tJoui2WgaZHYWEWyJOSx5Z27sx7k/54rQrsp01FeZ81i8XKvLT4QooorQ4wooooAKKKKACiiigAooooAKKKKACiiigAooooAKwvFPhez8UaYbe4/dzx5aC4Ay0bf1B7jv9cGt2ik0mrMqE5QkpRep80alpt7oWpy2F/F5c0Z5A5DDsynuD/wDW61JY3kllcC4gdlfY6AqcfeQr/X9K9z8V+FLPxTp3ky4iuosm3uAOUPofVT3H9a8Hv7C80XUZbC+hMc8Z+ZeoI7Mp7g+tclSm4O6PosJi44iHLLfqhpZmxuZmx3Zif500kKMnpTfNTGc/pV7RNEvvEuqpZWSc9ZJGHyxL/eP9B3rJJt2O6dSMI3bHaDoN94n1VbKzXAHzSysMrCnqfU+g7/nXvehaFZeHtLjsbGPai8u7fekbuzHuT/8AWpvh/QLLw5paWNknA+aSRvvSN3Zj/nHStWu2nT5V5nzWMxbrysvhCiiitDiCiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACud8WeErPxTYBJCIbuLJguAuSnsR3U9xXRUUmk1ZlQnKElKL1PE4/hR4ha78qSWxjizzOJWbj1C7Qfw4+tereHfDtl4a0tbKzXJ+9LKw+aVv7x/w7VrUVMacY7G9bF1aytJ6BRRRVnMFFFFABRRRQAUUUUAFFFFABRRRQB//Z
// @author            创客云IT
// @match             *://*.youku.com/*
// @match             *://*.iqiyi.com/*
// @match             *://*.iq.com/*
// @match             *://*.le.com/*
// @match             *://v.qq.com/*
// @match             *://m.v.qq.com/*
// @match             *://*.tudou.com/*
// @match             *://*.mgtv.com/*
// @match             *://tv.sohu.com/*
// @match             *://film.sohu.com/*
// @match             *://*.1905.com/*
// @match             *://*.bilibili.com/*
// @match             *://*.pptv.com/*
// @require           https://cdn.bootcdn.net/ajax/libs/jquery/3.7.1/jquery.min.js
// @require           https://cdn.bootcdn.net/ajax/libs/jquery/3.7.1/jquery.slim.min.js

// @grant             unsafeWindow
// @grant             GM_addStyle
// @grant             GM_openInTab
// @grant             GM_getValue
// @grant             GM_setValue
// @grant             GM_xmlhttpRequest
// @grant             GM_log
// @charset		      UTF-8
// @license           GPL License
// @downloadURL       https://gitlab.com/zheng8907/web_script/-/raw/main/VIP.user.js
// @updateURL         https://gitlab.com/zheng8907/web_script/-/raw/main/VIP.user.js
// ==/UserScript==

const util = (function () {

    function findTargetElement(targetContainer) {
        const body = window.document;
        let tabContainer;
        let tryTime = 0;
        const maxTryTime = 120;
        let startTimestamp;
        return new Promise((resolve, reject) => {
            function tryFindElement(timestamp) {
                if (!startTimestamp) {
                    startTimestamp = timestamp;
                }
                const elapsedTime = timestamp - startTimestamp;

                if (elapsedTime >= 500) {
                    GM_log("查找元素：" + targetContainer + "，第" + tryTime + "次");
                    tabContainer = body.querySelector(targetContainer);
                    if (tabContainer) {
                        resolve(tabContainer);
                    } else if (++tryTime === maxTryTime) {
                        reject();
                    } else {
                        startTimestamp = timestamp;
                    }
                }
                if (!tabContainer && tryTime < maxTryTime) {
                    requestAnimationFrame(tryFindElement);
                }
            }

            requestAnimationFrame(tryFindElement);
        });
    }

    function urlChangeReload() {
        const oldHref = window.location.href;
        let interval = setInterval(() => {
            let newHref = window.location.href;
            if (oldHref !== newHref) {
                clearInterval(interval);
                window.location.reload();
            }
        }, 500);
    }
    function reomveVideo() {
        setInterval(() => {
            for (let video of document.getElementsByTagName("video")) {
                if (video.src) {
                    video.removeAttribute("src");
                    video.muted = true;
                    video.load();
                    video.pause();
                }
            }
        }, 500);
    }

    function syncRequest(option) {
        return new Promise((resolve, reject) => {
            option.onload = (res) => {
                resolve(res);
            };
            option.onerror = (err) => {
                reject(err);
            };
            GM_xmlhttpRequest(option);
        });
    }

    return {
        req: (option) => syncRequest(option),
        findTargetEle: (targetEle) => findTargetElement(targetEle),
        urlChangeReload: () => urlChangeReload(),
        reomveVideo: () => reomveVideo()
    }
})();


const superVip = (function () {

    const _CONFIG_ = {
        isMobile: navigator.userAgent.match(/(Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini)/i),
        currentPlayerNode: null,
        vipBoxId: 'vip_jx_box' + Math.ceil(Math.random() * 100000000),
        flag: "flag_vip",
        autoPlayerKey: "auto_player_key" + window.location.host,
        autoPlayerVal: "auto_player_value_" + window.location.host,
        videoParseList: [
            {"name": "频道1", "type": "1,3", "url": "https://jx.xmflv.com/?url="},//虾米
            {"name": "频道2", "type": "1,3", "url": "https://jx.aidouer.net/?url="},//爱豆
            {"name": "频道3", "type": "1,3", "url": "https://www.yemu.xyz/?url="},//夜幕
            //{"name": "频道4", "type": "1,3", "url": "https://bd.jx.cn/?url="},//冰豆
           // {"name": "频道5", "type": "1,2", "url": "https://im1907.top/?jx="},//m1907
           // {"name": "频道6", "type": "1,2", "url": "https://jx.yparse.com/index.php?url="},//yparse
        ],
        playerContainers: [
            {
                host: "v.qq.com",
                container: "#mod_player,#player-container,.container-player",
                name: "Default",
                displayNodes: ["#mask_layer", ".mod_vip_popup", "#mask_layer", ".panel-tip-pay"]
            },
            {
                host: "m.v.qq.com",
                container: ".mod_player,#player",
                name: "Default",
                displayNodes: [".mod_vip_popup", "[class^=app_],[class^=app-],[class*=_app_],[class*=-app-],[class$=_app],[class$=-app]", "div[dt-eid=open_app_bottom]", "div.video_function.video_function_new", "a[open-app]", "section.mod_source", "section.mod_box.mod_sideslip_h.mod_multi_figures_h,section.mod_sideslip_privileges,section.mod_game_rec", ".at-app-banner"]
            },

            {host: "w.mgtv.com", container: "#mgtv-player-wrap", name: "Default", displayNodes: []},
            {host: "www.mgtv.com", container: "#mgtv-player-wrap", name: "Default", displayNodes: []},
            {
                host: "m.mgtv.com",
                container: ".video-area",
                name: "Default",
                displayNodes: ["div[class^=mg-app]", ".video-area-bar", ".open-app-popup"]
            },
            {host: "www.bilibili.com", container: "#player_module,#bilibiliPlayer,#bilibili-player", name: "Default", displayNodes: []},
            {host: "m.bilibili.com", container: ".player-wrapper,.player-container,.mplayer", name: "Default", displayNodes: []},
            {host: "www.iqiyi.com", container: "#flashbox", name: "Default", displayNodes: ["#playerPopup", "div[class^=qy-header-login-pop]", "section[class^=modal-cover_]" ,".toast"]},
            {
                host: "m.iqiyi.com",
                container: ".m-video-player-wrap",
                name: "Default",
                displayNodes: ["div.m-iqyGuide-layer", "a[down-app-android-url]", "[name=m-extendBar]", "[class*=ChannelHomeBanner]", "section.m-hotWords-bottom"]
            },
            {host: "www.iq.com", container: ".intl-video-wrap", name: "Default", displayNodes: []},
            {host: "v.youku.com", container: "#player", name: "Default", displayNodes: ["#iframaWrapper", "#checkout_counter_mask", "#checkout_counter_popup"]},
            {
                host: "m.youku.com",
                container: "#player,.h5-detail-player",
                name: "Default",
                displayNodes: [".callEnd_box", ".h5-detail-guide", ".h5-detail-vip-guide"]
            },
            {host: "tv.sohu.com", container: "#player", name: "Default", displayNodes: []},
            {host: "film.sohu.com", container: "#playerWrap", name: "Default", displayNodes: []},
            {host: "www.le.com", container: "#le_playbox", name: "Default", displayNodes: []},
            {host: "video.tudou.com", container: ".td-playbox", name: "Default", displayNodes: []},
            {host: "v.pptv.com", container: "#pptv_playpage_box", name: "Default", displayNodes: []},
            {host: "vip.pptv.com", container: ".w-video", name: "Default", displayNodes: []},
            {host: "www.wasu.cn", container: "#flashContent", name: "Default", displayNodes: []},
            {host: "www.acfun.cn", container: "#player", name: "Default", displayNodes: []},
            {host: "vip.1905.com", container: "#player,#vodPlayer", name: "Default", displayNodes: []},
            {host: "www.1905.com", container: "#player,#vodPlayer", name: "Default", displayNodes: []},
        ]
    };

    class BaseConsumer {
        constructor() {
            this.parse = () => {
                util.findTargetEle('body')
                    .then((container) => this.preHandle(container))
                    .then((container) => this.generateElement(container))
                    .then((container) => this.bindEvent(container))
                    .then((container) => this.autoPlay(container))
                    .then((container) => this.postHandle(container));
            }
        }

        preHandle(container) {
            _CONFIG_.currentPlayerNode.displayNodes.forEach((item, index) => {
                util.findTargetEle(item)
                    .then((obj) => obj.style.display = 'none')
                    .catch(e => console.warn("不存在元素", e));
            });
            return new Promise((resolve, reject) => resolve(container));
        }

        generateElement(container) {
            GM_addStyle(`
                        #${_CONFIG_.vipBoxId} {cursor:pointer; position:fixed; top:120px; left:0px; z-index:9999999; text-align:left;}
                        #${_CONFIG_.vipBoxId} .img_box{width:32px; height:32px;line-height:32px;text-align:center;background-color:#e04870;margin:10px 0px;}
                        #${_CONFIG_.vipBoxId} .vip_list {display:none; position:absolute; border-radius:5px; left:32px; top:0; text-align:center; background-color: #3f4149; border:1px solid white;padding:10px 0px; width:380px; max-height:400px; overflow-y:auto;}
                        #${_CONFIG_.vipBoxId} .vip_list li{border-radius:2px; font-size:12px; color:#18ba56; text-align:center; width:calc(25% - 14px); line-height:21px; float:left; border:1px solid gray; padding:0 4px; margin:4px 2px;overflow:hidden;white-space: nowrap;text-overflow: ellipsis;-o-text-overflow:ellipsis;}
                        #${_CONFIG_.vipBoxId} .vip_list li:hover{color:#1c84c6; border:1px solid #1c84c6;}
                        #${_CONFIG_.vipBoxId} .vip_list ul{padding-left: 10px;}
                        #${_CONFIG_.vipBoxId} .vip_list::-webkit-scrollbar{width:5px; height:1px;}
                        #${_CONFIG_.vipBoxId} .vip_list::-webkit-scrollbar-thumb{box-shadow:inset 0 0 5px rgba(0, 0, 0, 0.2); background:#A8A8A8;}
                        #${_CONFIG_.vipBoxId} .vip_list::-webkit-scrollbar-track{box-shadow:inset 0 0 5px rgba(0, 0, 0, 0.2); background:#F1F1F1;}
                        #${_CONFIG_.vipBoxId} li.selected{color:#1c84c6; border:1px solid #1c84c6;}
						`);

            if (_CONFIG_.isMobile) {
                GM_addStyle(`
                    #${_CONFIG_.vipBoxId} {top:300px;}
                    #${_CONFIG_.vipBoxId} .vip_list {width:300px;}
                    `);
            }

            let type_1_str = "";
            //let type_2_str = "";
            //let type_3_str = "";
            _CONFIG_.videoParseList.forEach((item, index) => {
                if (item.type.includes("1")) {
                    type_1_str += `<li class="nq-li" title="${item.name}1" data-index="${index}">${item.name}</li>`;
                }
            });

            let autoPlay = !!GM_getValue(_CONFIG_.autoPlayerKey, null) ? "开" : "关";

            $(container).append(`
                <div id="${_CONFIG_.vipBoxId}">
                    <div class="vip_icon">
                        <div class="img_box" title="选择解析源" style="color:white;font-size:16px;font-weight:bold;border-radius:5px;"><span style="color: #ccbc31;">V</span>I<span style="color: yellow;">P</span></div>
                        <div class="vip_list">
                            <div>
                                <h3 style="color:#FFFF; font-weight: bold; font-size: 16px; padding:5px 0px;">[播放频道]</h3>
                                <ul>
                                    ${type_1_str}
                                    <div style="clear:both;"></div>
                                </ul>
                            </div>
                            <div style="text-align:left;color:#FFFF;font-size:12px;padding:0px 12px;margin-top:12px;">
                                <b>自动解析功能说明：</b>
                                <br>&nbsp;&nbsp;1、自动解析功能打开，将根据当前所选频道进行自动解析播放。
                                <br>&nbsp;&nbsp;2、如当前没有选中频道系统将随机选取一个
                                <br>&nbsp;&nbsp;3、如当前频道解析播放失败，可手动选择其他的频道尝试。
                                <br>&nbsp;&nbsp;4、如当前网站已有会员可选择以关闭自动解析功能
                            </div>
                        </div>
                    </div>
                    <div class="img_box" id="vip_auto" style="color:white;font-size:16px;font-weight:bold;border-radius:5px;" title="自动解析功能当前状态：关！是否选择打开！！">${autoPlay}</div>
                </div>`);
            return new Promise((resolve, reject) => resolve(container));
        }

        bindEvent(container) {
            const vipBox = $(`#${_CONFIG_.vipBoxId}`);
            if (_CONFIG_.isMobile) {
                vipBox.find(".vip_icon").on("click", () => vipBox.find(".vip_list").toggle());
            } else {
                vipBox.find(".vip_icon").on("mouseover", () => vipBox.find(".vip_list").show());
                vipBox.find(".vip_icon").on("mouseout", () => vipBox.find(".vip_list").hide());
            }

            let _this = this;
            vipBox.find(".vip_list .nq-li").each((liIndex, item) => {
                item.addEventListener("click", () => {
                    const index = parseInt($(item).attr("data-index"));
                    GM_setValue(_CONFIG_.autoPlayerVal, index);
                    GM_setValue(_CONFIG_.flag, "true");
                    _this.showPlayerWindow(_CONFIG_.videoParseList[index]);
                    vipBox.find(".vip_list li").removeClass("selected");
                    $(item).addClass("selected");
                });
            });
            vipBox.find(".vip_list .tc-li").each((liIndex, item) => {
                item.addEventListener("click", () => {
                    const index = parseInt($(item).attr("data-index"));
                    const videoObj = _CONFIG_.videoParseList[index];
                    let url = videoObj.url + window.location.href;
                    GM_openInTab(url, {active: true, insert: true, setParent: true});
                });
            });

            //右键移动位置
            vipBox.mousedown(function (e) {
                if (e.which !== 3) {
                    return;
                }
                e.preventDefault()
                vipBox.css("cursor", "move");
                const positionDiv = $(this).offset();
                let distenceX = e.pageX - positionDiv.left;
                let distenceY = e.pageY - positionDiv.top;

                $(document).mousemove(function (e) {
                    let x = e.pageX - distenceX;
                    let y = e.pageY - distenceY;
                    const windowWidth = $(window).width();
                    const windowHeight = $(window).height();

                    if (x < 0) {
                        x = 0;
                    } else if (x > windowWidth - vipBox.outerWidth(true) - 100) {
                        x = windowWidth - vipBox.outerWidth(true) - 100;
                    }

                    if (y < 0) {
                        y = 0;
                    } else if (y > windowHeight - vipBox.outerHeight(true)) {
                        y = windowHeight - vipBox.outerHeight(true);
                    }
                    vipBox.css("left", x);
                    vipBox.css("top", y);
                });
                $(document).mouseup(function () {
                    $(document).off('mousemove');
                    vipBox.css("cursor", "pointer");
                });
                $(document).contextmenu(function (e) {
                    e.preventDefault();
                })
            });
            return new Promise((resolve, reject) => resolve(container));
        }

        autoPlay(container) {
            const vipBox = $(`#${_CONFIG_.vipBoxId}`);
            vipBox.find("#vip_auto").on("click", function () {
                if (!!GM_getValue(_CONFIG_.autoPlayerKey, null)) {
                    GM_setValue(_CONFIG_.autoPlayerKey, null);
                    $(this).html("关");
                    $(this).attr("title", "自动解析功能当前状态：关！是否选择打开！！");
                } else {
                    GM_setValue(_CONFIG_.autoPlayerKey, "true");
                    $(this).html("开");
                }
                setTimeout(function () {
                    window.location.reload();
                }, 200);
            });

            if (!!GM_getValue(_CONFIG_.autoPlayerKey, null)) {
                this.selectPlayer(container);
            }
            return new Promise((resolve, reject) => resolve(container));
        }

        selectPlayer(container) {
            let index = GM_getValue(_CONFIG_.autoPlayerVal, 2);
            let autoObj = _CONFIG_.videoParseList[index];
            let _th = this;
            if (autoObj.type.includes("1")) {
                setTimeout(function () {
                    _th.showPlayerWindow(autoObj);
                    const vipBox = $(`#${_CONFIG_.vipBoxId}`);
                    vipBox.find(`.vip_list [title="${autoObj.name}1"]`).addClass("selected");
                    $(container).find("#vip_auto").attr("title", `自动解析已开：${autoObj.name}`);
                }, 2500);
            }
        }

        showPlayerWindow(videoObj) {
            util.findTargetEle(_CONFIG_.currentPlayerNode.container)
                .then((container) => {
                    const type = videoObj.type;
                    let url = videoObj.url + window.location.href;
                    if (type.includes("1")) {
                        util.reomveVideo();
                        $(container).empty();
                        $(container).empty();
                        let iframeDivCss = "width:100%;height:100%;z-index:999999;";
                        if (_CONFIG_.isMobile) {
                            iframeDivCss = "width:100%;height:220px;z-index:999999;";
                        }
                        if (_CONFIG_.isMobile && window.location.href.indexOf("iqiyi.com") !== -1) {
                            iframeDivCss = "width:100%;height:220px;z-index:999999;margin-top:-56.25%;";
                        }
                        $(container).append(`<div style="${iframeDivCss}"><iframe id="iframe-player-4a5b6c" src="${url}" style="border:none;" allowfullscreen="true" width="100%" height="100%"></iframe></div>`);
                    }
                });
        }

        postHandle(container) {
            if (!!GM_getValue(_CONFIG_.autoPlayerKey, null)) {
                util.urlChangeReload();
            } else {
                let oldHref = window.location.href;
                let interval = setInterval(() => {
                    let newHref = window.location.href;
                    if (oldHref !== newHref) {
                        oldHref = newHref;
                        if (!!GM_getValue(_CONFIG_.flag, null)) {
                            clearInterval(interval);
                            window.location.reload();
                        }
                    }
                }, 1000);
            }
        }

    }

    class DefaultConsumer extends BaseConsumer {
    }

    return {
        start: () => {
            GM_setValue(_CONFIG_.flag, null);
            let mallCase = 'Default';
            let playerNode = _CONFIG_.playerContainers.filter(value => value.host === window.location.host);
            if (playerNode === null || playerNode.length <= 0) {
                console.warn(window.location.host + "该网站暂不支持，请联系作者，作者将会第一时间处理（注意：请记得提供有问题的网址）");
                return;
            }
            _CONFIG_.currentPlayerNode = playerNode[0];
            mallCase = _CONFIG_.currentPlayerNode.name;
            const targetConsumer = eval(`new ${mallCase}Consumer`);
            targetConsumer.parse();
        }
    }

})();

(function () {
    superVip.start();
})();