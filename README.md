<h1><img src="https://img.shields.io/badge/MIT-License-brightgreen.svg?style=popout-square" alt=""> <img src="https://img.shields.io/badge/Platform-Windows | Mac | Android | IOS-red.svg?style=popout-square" alt=""></h1>

<div align="center">
  <a href="https://gitlab.com/maker_cloud#%E8%84%9A%E6%9C%AC%E4%BB%8B%E7%BB%8D" target="_blank" rel="noopener noreferrer">
  <img width="raw/" src="https://gitee.com/zheng8907/web_script/raw/master/img/VIP.jpg" alt="Vite logo">
  </a>
</div>

<div align="center">
  <h2>全网VIP会员专享视频免费看</h2>
  <h2>声明</h2>
</div> 

1. VIP视频解析中所用到的解析接口全部收集自互联网（源码可见），版权问题请联系相关解析接口所有者!
2. 为创造良好的创作氛围，请大家支持正版！
3. 脚本仅限个人学习交流，切勿用于任何商业等其它用途！
4. 继续使用，即表明你已经明确使用脚本可能带来的风险，且愿意自行承担相关风险，对于风险脚本不承担任何责任！

<div align="center">


<h2>脚本简介</h2>
<h5>PC端完整版</h5>
此脚本精选解析线路为大家提供
优酷、爱奇艺、腾讯、B站(bilibili)、乐视、芒果等各大视频网站(PC+移动端)视频解析服务，
让你省去购买视频VIP费用，同时提供知乎增强(知乎视频下载、去广告、去除侧边栏、关键词屏蔽等会员功能)，
全网音乐和有声书免客户端下载(网易云音乐、QQ音乐、酷狗、酷我、虾米、蜻蜓FM、荔枝FM、喜马拉雅等)，
视频无水印下载(bilibili、抖音、快手、西瓜、youtube)，自动查券功能，所有功能互不影响，可独立开关。
<h5>移动端增强版</h5>
支持：腾讯、爱奇艺、优酷、芒果、pptv、乐视等其它网站会员视频播放和下载，
针对移动端同时兼容桌面网站和移动网站，本站内置播放，
可通过图标选择播放频道和设置开/关实现自动播放/手动播放功能。

</div>

## 安装目录

- <h4>[安装浏览器](#安装浏览器)</h4>
- <h4>[安装油猴tampermonkey插件](#油猴tampermonkey)</h4>
- <h4>[安装脚本](#安装脚本)</h4>

  - <h5>[完整版](#安装脚本)</h5>

  - <h5>[手机增强版](#安装脚本)</h5>
- <h4>[使用](#使用)</h4>

    - <h5>[PC端](#pc端)</h5>

    - <h5>[移动端](#移动端)</h5>

- <h4>[常见问题](#常见问题)</h4>
- <h4>[更新日志](#更新日志)</h4>

### 安装浏览器 & 油猴tampermonkey插件
- [ ] <h5>安装油猴插件前，请先确认需要安装的设备端是否已安装有对应的浏览器，如没有请根据自己设备安装一款浏览器</h5>

<details><summary>Windows 桌面版(展开)</summary>
<p>

- [ ] 对应安装一款即可
- [ ] 安装浏览器 [Google Chrome](https://www.google.cn/intl/zh-CN/chrome/next-steps.html?platform=linux&installdataindex=empty&defaultbrowser=0) & 安装插件 [Tampermonkey](https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo)

- [ ] 安装浏览器 [Mozilla Firefox](https://www.firefox.com.cn/) & 安装插件 [Tampermonkey](https://addons.mozilla.org/firefox/addon/tampermonkey/)


- [ ] 安装浏览器 [Microsoft Edge](https://www.microsoft.com/zh-cn/edge/download?form=MA13FJ) & 安装插件 [Tampermonkey](https://microsoftedge.microsoft.com/addons/detail/%E7%AF%A1%E6%94%B9%E7%8C%B4/iikmkjmpaadaobahmlepeloendndfphd)
</p>
</details>
<details><summary>MAC(展开)</summary>
<p>

- [ ] Safari浏览器系统自带 & 安装插件 [Tampermonkey](https://www.tampermonkey.net/?browser=safari)
- [ ] 安装浏览器 [Google Chrome](https://www.google.cn/intl/zh-CN/chrome/next-steps.html?platform=linux&installdataindex=empty&defaultbrowser=0) & 安装插件 [Tampermonkey](https://www.tampermonkey.net/index.php?browser=chrome)

- [ ] 安装浏览器 [Mozilla Firefox](https://www.firefox.com.cn/) & 安装插件 [Tampermonkey](https://addons.mozilla.org/en-US/firefox/addon/tampermonkey/)

- [ ] 安装浏览器 [Microsoft Edge](https://www.microsoft.com/zh-cn/edge?form=MA13FJ) & 安装插件 [Tampermonkey](https://microsoftedge.microsoft.com/addons/detail/%E7%AF%A1%E6%94%B9%E7%8C%B4/iikmkjmpaadaobahmlepeloendndfphd)

</p>
</details>

<details><summary>Android(展开)</summary>
<p>

- [ ] 用手机扫码，先装浏览器，再装插件

| <div align="center">浏览器 & 插件 | <div align="center">浏览器 & 插件 |
| ------ | ------ |
| [Firefox & Tampermonkey](https://www.mozilla.org/zh-CN/firefox/all/#product-desktop-release) | [Lemur & Tampermonkey](https://microsoftedge.microsoft.com/addons/detail/%E7%AF%A1%E6%94%B9%E7%8C%B4/iikmkjmpaadaobahmlepeloendndfphd) |
|<img src="https://gitee.com/zheng8907/web_script/raw/master/img/Firefox2.png" height=150px></a></div>|<img src="https://gitee.com/zheng8907/web_script/raw/master/img/Lemur2.png" height=150px></a></div>|

</p>
</details>

<details><summary>IOS(展开)</summary>
<p>

 可在IOS应用商店中搜索并安装 “stay”或者“Addons”，不要用“UserScripts”。

- [ ] 插件Install [Stay for Safari](https://apps.apple.com/cn/app/stay-for-safari-%E6%B5%8F%E8%A7%88%E5%99%A8%E4%BC%B4%E4%BE%A3/id1591620171)

- [ ] 插件Install [Addons](https://apps.apple.com/cn/app/addons-%E6%B5%8F%E8%A7%88%E5%99%A8%E6%89%A9%E5%B1%95%E6%8F%92%E4%BB%B6%E5%B9%BF%E5%91%8A%E6%8B%A6%E6%88%AA/id6446811843)

</p>
</details>

<details><summary>其他浏览器(展开)</summary>
<p>

其他浏览器可在官方扩展市场搜索: “Tampermonkey”、“篡改猴”、“油猴”、“暴力猴”等脚本插件进行安装。

</p>
</details>

## 安装脚本
   
<div align="center">

| <div align="center"><h3>PC完整版</h3></div> | <div align="center"><h3>移动端增强版</h3></div> |
| ------------------------------------------ | --------------------------------------------- |
|<div align="center"><a href="https://gitlab.com/zheng8907/web_script/-/raw/main/CK.user.js" rel="nofollow"><img src="https://img.shields.io/badge/Ver-6.7.2-red.svg?style=popout-square" height=25px></a></div>|<div align="center"><a href="https://gitlab.com/zheng8907/web_script/-/raw/main/VIP.user.js" rel="nofollow"><img src="https://img.shields.io/badge/Ver-2.1.2-red.svg?style=popout-square" height=25px /></a></div>
|<a href="https://gitlab.com/zheng8907/web_script/-/raw/main/CK.user.js" rel="nofollow"><img src="https://img.shields.io/badge/Download-brightgreen.svg?style=popout-square" height=35px /></a>|<a href="https://gitlab.com/zheng8907/web_script/-/raw/main/VIP.user.js" rel="nofollow"><img src="https://img.shields.io/badge/Download-brightgreen.svg?style=popout-square" height=35px /></a>|
| <img src="https://gitee.com/zheng8907/web_script/raw/master/img/CK2.png?style=popout-square" height=250px></a> | <img src="https://gitee.com/zheng8907/web_script/raw/master/img/VIP2.png?style=popout-square" height=250px></a> |
| 扫码安装 | 扫码安装 |

</div> 

## 使用

#### PC端

<details><summary>脚本设置(重要)(展开)</summary>
<p>

![脚本设置](https://gitlab.com/zheng8907/web_script/-/raw/main/img/12.png)
![](https://gitlab.com/zheng8907/web_script/-/raw/main/img/2.png)

    浏览器打开任意脚本可加载的网站，例如优酷、知乎、b站等，
    在右上角Tampermonkey中打开脚本设置。
    点击开关右侧 ">" 按钮可进入子设置。

</p>
</details>

<details><summary>具体功能使用(展开)</summary>
<p>

一，视频解析

![解析图标](https://gitlab.com/zheng8907/web_script/-/raw/main/img/zhanwai.jpg)

    站内解析:
    1. 脚本“设置”-“解析设置”-“站外解析”调整为关闭状态
    2. 浏览任意单个视频，鼠标移动到左侧红色VIP图标上，弹出的窗口选择合适的线路点击即可在网页内播放。
   
    站外解析:
    3. 脚本“设置”-“解析设置”-“站外解析”调整为开启状态
    4. 浏览任意单个视频，鼠标移动到左侧红色VIP图标上，弹出的窗口选择合适的线路点击即可打开新页面播放。
   
    自定义线路:
    5. 脚本“设置”-“解析设置”-“解析线路”
    6. 编辑线路内容，每线路一行，线路名称和线路地址用半角逗号隔开。

二，视频下载

![抖音下载](https://gitlab.com/zheng8907/web_script/-/raw/main/img/douyin.jpg)

    1. 在douyin.com/kuaishou.com/xigua.com/bilibili.com任意视频播放窗口的下方播放工具条找到下载按钮
    2. 选择对应的下载方式点击就可以下载
    3. 注意: 直接下载方式，越长的视频需要等待的时间越长。
    4. 注意: bilibili屏蔽了网页方式直接下载，这里可以安装IDM(Internet Download Manager)，在点击下载的同时唤醒IDM进行下载就能成功。

三，Youtube下载

    1. 鼠标移动到youtube.com任意视频播放窗口左侧红色VIP图标
    2. 选择对应的下载线路点击
    3. 在弹出的页面中点击下载。

四，知乎增强

    在脚本设置中开关相关功能。

五，购物助手

    1. 筛选仅显示淘宝: 在淘宝商品搜索页面，默认只有仅显示天猫，没有仅显示淘宝。脚本新增仅显示淘宝功能，勾选即可方便筛选商品。
    2. 显示隐藏优惠券: 淘宝、天猫、京东等商品页面立即购买上方显示隐藏优惠券。

六，音乐下载

    网易云音乐
    1. 点击任意一个音乐单曲(非歌单)
    2. 点击左侧VIP图标
    3. 在弹出的窗口中点下载。

    腾讯音乐
    1. 点击任意一个音乐单曲，点击播放
    2. 在播放页面点击左侧VIP图标
    3. 在弹出的窗口中点下载。
    
    酷狗/酷我音乐
    1. 点击任意一个音乐单曲，点击播放
    2. 在播放页面下方的进度条上找到下载按钮，点击下载。
  
    喜马拉雅
    单独下载:
    1. 点击任意一个音乐单曲，点击播放
    2. 在播放页面下方的进度条上找到下载按钮，点击下载。
    批量下载:
    1. 在音乐集的音乐列表左侧勾选需要下载的音乐
    2. 点击音乐集音乐列表右上方批量下载。
</p>
</details>

#### 移动端

<details><summary>观看VIP视频(展开)</summary>
<p>

<div align="center">
<a><img src="https://gitlab.com/zheng8907/web_script/-/raw/main/img/IMG_2666.png?ref_type=heads&inline=false" height=700px></a>
</div>

    浏览任意单个视频，鼠标点击左侧红色VIP图标，在弹出的窗口选择合适的线路点击即可在网页内播放。
</p>
</details>

## 常见问题

<details><summary>安装相关(展开)</summary>
<p>

1. 不显示VIP图标怎么排查？<br>
* 脚本只支持浏览器，不支持客户端，且只会在能起作用的页面显示。例如zhihu.com
* 点击浏览器右上角的Tampermonkey插件图标，查看是否为已启用状态。
* 点击浏览器右上角的Tampermonkey插件图标，在管理面板里关闭其他脚本，按F5刷新网页试试。
* 如果是360安全浏览器要切换到极速模式，另外不建议使用国产浏览器。
* 点击浏览器右上角的Tampermonkey插件图标，打开脚本的设置，查看图标设置是否正常，根据需要调整图标的位置并保存。
* 以上都没有解决，可以在浏览器右上角点击Tampermonkey，进入“管理面板”，在右上角“已安装脚本”页面删除所有脚本，进入右上角“设置”和“实用工具”中间的“回收站”，删除所有脚本，再重新安装最新版[全网VIP工具箱](https://gitlab.com/zheng8907/web_script/-/raw/main/CK.user.js)，基本可以解决。

2. 显示多个VIP图标怎么办？
可能是你同时安装了多个版本的脚本，造成每个脚本生成了一个VIP图标。关闭其他版本，只保留最新版[全网VIP工具箱](https://gitlab.com/zheng8907/web_script/-/raw/main/CK.user.js)即可解决。

3. safari浏览器怎么使用脚本？<br>
safari可以使用Tampermonkey，但是因为浏览器内核不一样，使用中可能会有问题。

4. 为什么我的浏览器点击安装脚本会下载一个文件，打开会提示编译错误？<br>
安装脚本前应该先安装Tampermonkey，点击安装脚本会自动安装到Tampermonkey里，如果已经安装Tampermonkey还出现这样的情况，重启试试，或者通过手动安装的方式安装。

5. 为什么已经卸载了脚本还会在视频网站左侧显示图标?<br>
检查该浏览器是否安装了暴力猴，Greasemonkey，AdGuard或者其他同类插件，可能是这些插件里也安装了脚本，删除这些插件里的脚本试试。

6. 电视上能不能用?<br>
一般情况不可以，有能力的可以自行研究。

7. 脚本弹出更新提示怎么办？<br>
脚本在发现新版本后会自动弹出更新提示。点击“忽略”则当天不会再有提示；点击“查看更新”并更新到[最新版](https://raw.kkgithub.com/Zheng8907/silver-waffle/main/CK.user.js)的脚本则不会再有提示。 [点击更新](https://raw.kkgithub.com/Zheng8907/silver-waffle/main/CK.user.js)

</p>
</details>

<details><summary>视频音乐相关(展开)</summary>
<p>

* 脚本没反应怎么排查？<br>
  按以下顺序排查：
1. 浏览器右上角的油猴插件里的脚本设置是否正常显示？<br>
如果不能正常显示，则脚本安装或加载有问题。删除脚本重新安装试试。如果还不行，欢迎反馈。
2. VIP图标是否显示？<br>
如果不显示，则可能是其他插件或者脚本冲突。关闭其他脚本并刷新页面试试，或者换一个浏览器试试。如果还不行，则可能是脚本问题，欢迎反馈。
3. 鼠标移动到VIP图标，是否弹出线路窗口？<br>
脚本在单独视频页面才会有弹出窗口，在视频列表页面则不会弹出。如果在单独视频页面也不弹出窗口，则为脚本问题，欢迎反馈。
4. 点击窗口线路，视频是否有反应？<br>
如果原视频被替换，则脚本正常。否则为不正常，欢迎反馈。同时也可以依次点击浏览器右上角“Tampermonkey”-“懒人工具箱”-“设置”-“视频解析”右边的“>”，打开“站外解析”并关闭窗口，使用站外解析来解决。
5. 点击窗口线路，视频是否显示解析失败？<br>
如果显示解析失败等提示，则为线路或者网络问题，到这一步已经排除脚本问题，继续下一步。
6. 同一个视频，点击窗口中其他线路，视频是否显示解析失败？<br>
如果所有线路均显示解析失败，可能是视频解析问题，一般较新的视频会出现这种情况，过几天等线路同步了再解析即可。
7. 换一个其他较旧视频，并尝试点击窗口中多个线路，是否都显示解析失败？<br>
如果全部解析失败，则基本可以判断是网络问题，解析线路不带cdn，要靠你的网络硬实力。换个ip或者使用vpn试试，应该能解决。
8. 换其他视频网站，选择多个视频，尝试点击多个窗口线路，是否都显示解析失败？<br>
这个操作用来判断线路是否在某个视频网站上解析失败。

* 为什么以前用的好好的现在不能用了？<br>
一般晚上用的人较多，线路会比较拥挤。另外如果大量重复点击解析，可能会被线路认为是恶意行为，造成IP被解析线路屏蔽，换其他线路或者换个IP试试，也可以用vpn试试。另外目前脚本支持自定义线路，个人有好的线路可自行添加。

* 视频解析怎么看蓝光4K？<br>
视频解析的清晰度是各线路自己设置的，看不了蓝光4K。

* 为什么手机看视频会有广告？<br>
脚本已经把手机端带广告的线路屏蔽了，但是有可能之前没广告的线路，后来又添加了广告，脚本会定期更换这部分线路。

* 为什么B站视频下载打不开？<br>
脚本获取视频链接正常，但是B站屏蔽了浏览器直接下载视频，可以安装IDM(Internet Download Manager)，在点击下载的同时唤醒IDM进行下载就能成功。

* 音乐/视频下载位置在哪？<br>
你的操作系统下载目录，或者浏览器单独设置文件下载目录，在浏览器设置的下载历史里可以找到文件。

* 为什么有的音乐下载链接是空的？<br>
脚本的音乐下载功能是免客户端下载，也可以下载部分能在线听的歌曲，但无法下载收费歌曲。

* 为什么下载音乐会变成播放音乐？<br>
因为现在的浏览器都会自动播放下载的mp3文件，复制播放音乐页面的地址，放到下载软件例如迅雷、idm中下载即可。

* 为什么有的视频下载会提示加密？<br>
有的短视频网站会在用户预加载视频的时候判断网速，如果网速不够，就会用加密的方式加载视频。解决方法是分享视频，然后单独打开分享链接，默认首个视频一般都不会是加密视频，除非视频时间很长。

</p>
</details>

## 更新日志

<details><summary>日志(展开)</summary>
<p>
    6.7.0 修复部分网站解析和图标bug
</p>
</details>

---

[![Telegram](https://img.shields.io/badge/Telegram-issues-blue?logo=telegram)](https://t.me/+sGo6ZZvy54wzYTll)  [![Install](https://img.shields.io/badge/更新脚本-005200)](#安装脚本)
